## Small blog

## Task

Create small blog.

Blog Fields (only 4)

-Blog Title

-Blog Body Text

-Photo

-Date Posted

Display, Add, Edit and Delete Functions


## Start
- Create database

- Edit config .env file

- Install packages:

```bash
$ composer install

```
- Generate unique key:

```bash
$ php artisan key:generate

```

- Create tables:

```bash
$ php artisan migrate

```

- Create link
```bash
$ php artisan storage:link

```

