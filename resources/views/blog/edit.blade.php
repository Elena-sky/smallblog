@extends('template')

@section('content')
    <!-- Post Content Column -->
    <div class="card post-bloc">
        <h5 class="card-header">Редактировать запись</h5>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::model($post, array('route' => array('blog.update', $post->id), 'files' => true)) !!}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                {!! Form::label('title', 'Название') !!}
                {!! Form::text('title', $post->title, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('body_text', 'Текст') !!}
                {!! Form::text('body_text', $post->body_text, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group ">
                {!! Form::label('currentLogo', 'Текущее изображение:') !!}
                <div class="col-sm-12">
                    <img class="img-fluid rounded" src="{{asset($post->photo)}}" width="300px" alt="photo"/>
                </div>
            </div>
            <div class="form-group ">
                {!! Form::label('photo', 'Загрузите новое изображение:') !!}
                {!! Form::file('photo')!!}
            </div>
            <div class="form-group">
                {{ Form::button('Сохранить', ['class' => 'btn btn-success', 'type' => 'submit']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection