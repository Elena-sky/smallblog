@extends('template')

@section('content')
    <!-- Post Content Column -->
    <div class="card post-bloc">
        <h5 class="card-header">Новая запись</h5>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('route' => array('blog.store'), 'files' => true)) !!}
            <div class="form-group">
                {!! Form::label('title', 'Название:') !!}
                {!! Form::text('title', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('body_text', 'Текст:') !!}
                {!! Form::text('body_text', '', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group ">
                {!! Form::label('photo', 'Изображение:') !!}
                {!! Form::file('photo')!!}
            </div>
            <div class="form-group">
                {{ Form::button('Создать', ['class' => 'btn btn-success', 'type' => 'submit']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection