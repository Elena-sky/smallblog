@extends('template')

@section('content')
    @foreach($posts as $post)
        <div class="card my-4" id="{{$post->id}}">
            <h5 class="card-header"> {{$post->title}} </h5>
            <div class="card-body">
                <!-- Preview Image -->
                <img class="img-fluid rounded" src="{{asset($post->photo)}}" width="300px" alt="photo"/>
                <hr>
                <!-- Post Content -->
                <p class="lead"> {{$post->body_text}} </p>
                <hr>
                <!-- Date/Time -->
                <p>Создана {{$post->created_at}}</p>
                <hr>
                <a href=" {{ route('blog.edit', $post->id) }} ">
                    <button type="button" class="btn btn-warning">Редактировать</button>
                </a>
                <button type="button" data-post-id="{{$post->id}}" data-url="" class="btn btn-danger post">Удалить
                </button>
            </div>
        </div>
    @endforeach
    {{$posts->links()}}

    @empty($post)
        <div class="card my-4">
            <div class="card-body">
                <h5> Блог пока что пустой</h5>
            </div>
        </div>
    @endempty
@endsection