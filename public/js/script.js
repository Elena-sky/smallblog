$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.post').click(function () {
        var id = $(this).data('post-id'),
            url = $(this).data('url');
        if (confirm('Are you sure you want to delete?')) {
            $.ajax({
                type: 'DELETE',
                url: '/blog/'+ id,
                success: function (del) {
                    $('#'+id).remove();
                },
                error: function () {
                    console.log("ошибка");
                }
            });
        }
    });

});


