<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\CheckBlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Blog::query()
            ->orderBy('id', 'desc')
            ->paginate(3);
        $posts->map(function ($name) {
        return $name->photo = url(Storage::url($name->photo));
        });

        return view('blog.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckBlogPost $request)
    {
        $photo = $request->file('photo');
        if ($photo !== null){
            $photo = Storage::disk('public')->put('photo', $photo); // save the photo
        } else {
            $photo = 'no_picture.jpg';
        }

        $data = [
            'title' => $request['title'],
            'body_text' => $request['body_text'],
            'photo' => $photo,
        ];
        Blog::create($data);

        //Display a successful message
        return redirect()->route('blog.create')
            ->with('status', 'Запись - ' . $request['title'] . ' сохранена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Blog::find($id);
        $post->photo = url(Storage::url($post->photo));

        return view('blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Blog::find($id);

        if($request->hasFile('photo')){
            Storage::disk('public')->delete($post['photo']); // delete old photo
            $newPhoto = $request->file('photo'); // get new photo
            $photo = Storage::disk('public')->put('photo', $newPhoto); // save the photo
        } else {
            $photo = $post['photo'];
        }

        $data = [
            'title' => $request['title'],
            'body_text' => $request['body_text'],
            'photo' => $photo,
        ];

        $post->update($data);

        //Display a successful message
        return redirect()->route('blog.edit', $id)
            ->with('status', 'Запись - ' . $request['title'] . ' отредактирована');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Blog::find($id);
        $oldPhoto = $post['photo']; // find old photo
        Storage::disk('public')->delete($oldPhoto); // delete old photo

        Blog::destroy($id);
    }
}
